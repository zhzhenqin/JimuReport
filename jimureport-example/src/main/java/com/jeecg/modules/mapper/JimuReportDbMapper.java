package com.jeecg.modules.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jeecg.modules.entity.JimuReportDb;

/**
 * QueryApiMapper接口
 *
 * @author yiidata
 * @date 2022-12-06
 */
public interface JimuReportDbMapper extends BaseMapper<JimuReportDb>
{
    /**
     * 查询QueryApi
     *
     * @param id QueryApi主键
     * @return QueryApi
     */
    public JimuReportDb selectJimuReportDbById(String id);

    /**
     * 查询QueryApi列表
     *
     * @param jimuReportDb QueryApi
     * @return QueryApi集合
     */
    public List<JimuReportDb> selectJimuReportDbList(JimuReportDb jimuReportDb);

    /**
     * 新增QueryApi
     *
     * @param jimuReportDb QueryApi
     * @return 结果
     */
    public int insertJimuReportDb(JimuReportDb jimuReportDb);

    /**
     * 修改QueryApi
     *
     * @param jimuReportDb QueryApi
     * @return 结果
     */
    public int updateJimuReportDb(JimuReportDb jimuReportDb);

    /**
     * 删除QueryApi
     *
     * @param id QueryApi主键
     * @return 结果
     */
    public int deleteJimuReportDbById(String id);
}
