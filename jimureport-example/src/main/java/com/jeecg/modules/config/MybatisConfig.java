/**
 * DAP疫情数据分析平台
 *
 * http://www.9sdata.cn
 *
 * 版权所有，侵权必究！
 */

package com.jeecg.modules.config;

import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import com.yiidata.dataops.common.template.DatabaseRecognizer;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import javax.sql.DataSource;

/**
 * mybatis-plus配置
 *
 * @author zhenqin
 */
@Slf4j
@Configuration
@MapperScan(value = {
        "com.jeecg.modules.mapper",
})
public class MybatisConfig {

    @Bean(name = "sqlSessionFactory")
    public SqlSessionFactory sqlSessionFactory(DataSource dataSource)
            throws Exception {
        final MybatisSqlSessionFactoryBean sessionFactory = new MybatisSqlSessionFactoryBean();
        sessionFactory.setDataSource(dataSource);

        // 获取db
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        Resource[]  statementLocations = resolver.getResources("classpath*:mapper/*.xml");
        sessionFactory.setMapperLocations(statementLocations);
        return sessionFactory.getObject();
    }


}
