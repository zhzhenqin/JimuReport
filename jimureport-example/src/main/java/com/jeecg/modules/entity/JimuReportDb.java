package com.jeecg.modules.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;

import java.util.Date;

/**
 * QueryApi对象 jimu_report_db
 *
 * @author yiidata
 * @date 2022-12-06
 */
@TableName("jimu_report_db")
public class JimuReportDb
{
    private static final long serialVersionUID = 1L;

    /** id */
    @TableField(value = "id")
    private String id;

    /** 主键字段 */
    @TableField(value = "jimu_report_id")
    private String jimuReportId;

    /** 数据集编码 */
    @TableField(value = "db_code")
    private String dbCode;

    /** 数据集名字 */
    @TableField(value = "db_ch_name")
    private String dbChName;

    /** 数据源类型 */
    @TableField(value = "db_type")
    private String dbType;

    /** 数据库表名 */
    @TableField(value = "db_table_name")
    private String dbTableName;

    /** 动态查询SQL */
    @TableField(value = "db_dyn_sql")
    private String dbDynSql;

    /** 数据源KEY */
    @TableField(value = "db_key")
    private String dbKey;

    /** 填报数据源 */
    @TableField(value = "tb_db_key")
    private String tbDbKey;

    /** 填报数据表 */
    @TableField(value = "tb_db_table_name")
    private String tbDbTableName;

    /** java类数据集  类型（spring:springkey,class:java类名） */
    @TableField(value = "java_type")
    private String javaType;

    /** java类数据源  数值（bean key/java类名） */
    @TableField(value = "java_value")
    private String javaValue;

    /** 请求地址 */
    @TableField(value = "api_url")
    private String apiUrl;

    /** 请求方法0-get,1-post */
    @TableField(value = "api_method")
    private String apiMethod;

    /** 是否是列表0否1是 默认0 */
    @TableField(value = "is_list")
    private String isList;

    /** 是否作为分页,0:不分页，1:分页 */
    @TableField(value = "is_page")
    private String isPage;

    /** 数据源 */
    @TableField(value = "db_source")
    private String dbSource;

    /** 数据库类型 MYSQL ORACLE SQLSERVER */
    @TableField(value = "db_source_type")
    private String dbSourceType;

    /** json数据，直接解析json内容 */
    @TableField(value = "json_data")
    private String jsonData;

    /** api转换器 */
    @TableField(value = "api_convert")
    private String apiConvert;

    /** api Headers */
    @TableField(value = "api_headers")
    private String apiHeaders;

    /** 创建者 */
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 更新者 */
    private String updateBy;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;


    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }

    public void setJimuReportId(String jimuReportId)
    {
        this.jimuReportId = jimuReportId;
    }

    public String getJimuReportId()
    {
        return jimuReportId;
    }

    public void setDbCode(String dbCode)
    {
        this.dbCode = dbCode;
    }

    public String getDbCode()
    {
        return dbCode;
    }

    public void setDbChName(String dbChName)
    {
        this.dbChName = dbChName;
    }

    public String getDbChName()
    {
        return dbChName;
    }

    public void setDbType(String dbType)
    {
        this.dbType = dbType;
    }

    public String getDbType()
    {
        return dbType;
    }

    public void setDbTableName(String dbTableName)
    {
        this.dbTableName = dbTableName;
    }

    public String getDbTableName()
    {
        return dbTableName;
    }

    public void setDbDynSql(String dbDynSql)
    {
        this.dbDynSql = dbDynSql;
    }

    public String getDbDynSql()
    {
        return dbDynSql;
    }

    public void setDbKey(String dbKey)
    {
        this.dbKey = dbKey;
    }

    public String getDbKey()
    {
        return dbKey;
    }

    public void setTbDbKey(String tbDbKey)
    {
        this.tbDbKey = tbDbKey;
    }

    public String getTbDbKey()
    {
        return tbDbKey;
    }

    public void setTbDbTableName(String tbDbTableName)
    {
        this.tbDbTableName = tbDbTableName;
    }

    public String getTbDbTableName()
    {
        return tbDbTableName;
    }

    public void setJavaType(String javaType)
    {
        this.javaType = javaType;
    }

    public String getJavaType()
    {
        return javaType;
    }

    public void setJavaValue(String javaValue)
    {
        this.javaValue = javaValue;
    }

    public String getJavaValue()
    {
        return javaValue;
    }

    public void setApiUrl(String apiUrl)
    {
        this.apiUrl = apiUrl;
    }

    public String getApiUrl()
    {
        return apiUrl;
    }

    public void setApiMethod(String apiMethod)
    {
        this.apiMethod = apiMethod;
    }

    public String getApiMethod()
    {
        return apiMethod;
    }

    public void setIsList(String isList)
    {
        this.isList = isList;
    }

    public String getIsList()
    {
        return isList;
    }

    public void setIsPage(String isPage)
    {
        this.isPage = isPage;
    }

    public String getIsPage()
    {
        return isPage;
    }

    public void setDbSource(String dbSource)
    {
        this.dbSource = dbSource;
    }

    public String getDbSource()
    {
        return dbSource;
    }

    public void setDbSourceType(String dbSourceType)
    {
        this.dbSourceType = dbSourceType;
    }

    public String getDbSourceType()
    {
        return dbSourceType;
    }

    public void setJsonData(String jsonData)
    {
        this.jsonData = jsonData;
    }

    public String getJsonData()
    {
        return jsonData;
    }

    public void setApiConvert(String apiConvert)
    {
        this.apiConvert = apiConvert;
    }

    public String getApiConvert()
    {
        return apiConvert;
    }

    public String getApiHeaders() {
        return apiHeaders;
    }

    public void setApiHeaders(String apiHeaders) {
        this.apiHeaders = apiHeaders;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("jimuReportId", getJimuReportId())
                .append("createBy", getCreateBy())
                .append("updateBy", getUpdateBy())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .append("dbCode", getDbCode())
                .append("dbChName", getDbChName())
                .append("dbType", getDbType())
                .append("dbTableName", getDbTableName())
                .append("dbDynSql", getDbDynSql())
                .append("dbKey", getDbKey())
                .append("tbDbKey", getTbDbKey())
                .append("tbDbTableName", getTbDbTableName())
                .append("javaType", getJavaType())
                .append("javaValue", getJavaValue())
                .append("apiUrl", getApiUrl())
                .append("apiMethod", getApiMethod())
                .append("isList", getIsList())
                .append("isPage", getIsPage())
                .append("dbSource", getDbSource())
                .append("dbSourceType", getDbSourceType())
                .append("jsonData", getJsonData())
                .append("apiConvert", getApiConvert())
                .toString();
    }
}
