@echo off
@setlocal enableextensions enabledelayedexpansion

set CURRENT_DIR=%~dp0..

rem -----------------------------------------------------------
rem start jimureport(windows)
rem -----------------------------------------------------------

set MAINCLASS=com.jeecg.modules.JimuReportApplication
set LIB_DIR=lib
set CLASS_DIR=conf
set PRO_ARGS=
set JVM_ARGS=-Dapp.home=%CURRENT_DIR%\ -Djava.library.path=%CURRENT_DIR%\conf
set APP_HOME=%CURRENT_DIR%\

set jcommand=java.exe
set runtype=-classpath
set LIB_DIR=%CURRENT_DIR%\%LIB_DIR%
set CLASSES_DIR=%CURRENT_DIR%\%CLASS_DIR%;%LIB_DIR%\*
set APPEND_LIB=%CLASSES_DIR%
set FILE_SUFFIX=*.jar

@echo CURRENT_DIR=%CURRENT_DIR%
@echo off

%jcommand% %JVM_ARGS% %runtype% %APPEND_LIB% %MAINCLASS% %PRO_ARGS%