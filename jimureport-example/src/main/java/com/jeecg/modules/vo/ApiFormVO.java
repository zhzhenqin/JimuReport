package com.jeecg.modules.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * <pre>
 *
 * Created by zhenqin.
 * User: zhenqin
 * Date: 2022/12/6
 * Time: 下午6:59
 * Vendor: yiidata.com
 *
 * </pre>
 *
 * @author zhenqin
 */
@Setter
@Getter
public class ApiFormVO {

    /**
     * HTTP API URL
     */
    String api;

    /**
     * HTTP 执行函数
     */
    String method;

    /**
     * 转换
     */
    String apiConvert;

    String paramArray;

    /**
     * 需要的 Header
     */
    String headers;
}
