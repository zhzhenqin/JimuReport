package com.jeecg.modules.service.impl;

import java.util.List;
import com.yiidata.intergration.common.utils.DateUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import org.springframework.stereotype.Service;
import com.jeecg.modules.mapper.JimuReportDbMapper;
import com.jeecg.modules.entity.JimuReportDb;
import com.jeecg.modules.service.JimuReportDbService;

/**
 * QueryApiService业务层处理
 *
 * @author yiidata
 * @date 2022-12-06
 */
@Service
public class JimuReportDbServiceImpl extends ServiceImpl<JimuReportDbMapper, JimuReportDb> implements JimuReportDbService
{

    /**
     * 查询QueryApi
     *
     * @param id QueryApi主键
     * @return QueryApi
     */
    @Override
    public JimuReportDb selectJimuReportDbById(String id)
    {
        return this.getBaseMapper().selectJimuReportDbById(id);
    }

    /**
     * 查询QueryApi列表
     *
     * @param jimuReportDb QueryApi
     * @return QueryApi
     */
    @Override
    public List<JimuReportDb> selectJimuReportDbList(JimuReportDb jimuReportDb)
    {
        return this.getBaseMapper().selectJimuReportDbList(jimuReportDb);
    }

    /**
     * 新增QueryApi
     *
     * @param jimuReportDb QueryApi
     * @return 结果
     */
    @Override
    public int insertJimuReportDb(JimuReportDb jimuReportDb)
    {
        jimuReportDb.setCreateTime(DateUtils.getNowDate());
        return this.getBaseMapper().insertJimuReportDb(jimuReportDb);
    }

    /**
     * 修改QueryApi
     *
     * @param jimuReportDb QueryApi
     * @return 结果
     */
    @Override
    public int updateJimuReportDb(JimuReportDb jimuReportDb)
    {
        jimuReportDb.setUpdateTime(DateUtils.getNowDate());
        return this.getBaseMapper().updateJimuReportDb(jimuReportDb);
    }

    /**
     * 删除QueryApi信息
     *
     * @param id QueryApi主键
     * @return 结果
     */
    @Override
    public int deleteJimuReportDbById(String id)
    {
        return this.getBaseMapper().deleteJimuReportDbById(id);
    }
}
