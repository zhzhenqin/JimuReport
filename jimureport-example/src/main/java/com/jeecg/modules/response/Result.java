package com.jeecg.modules.response;

import com.alibaba.fastjson.JSON;

/**
 *
 * Api 返回结果对象
 *
 * @author zhenqin
 *
 * @date 2018/07/15
 */
public class Result<T> {

  private Integer code;

  private String message;

  private T result;


  private long timestamp = System.currentTimeMillis();

  @Override
  public String toString() {
    return JSON.toJSONString(this);
  }

  public Integer getCode() {
    return this.code;
  }

  public Result<T> setCode(final Integer code) {
    this.code = code;
    return this;
  }

  public String getMessage() {
    return this.message;
  }


  public String getMsg() {
    return this.message;
  }


  public Result<T> setMessage(final String message) {
    this.message = message;
    return this;
  }

  public T getResult() {
    return this.result;
  }

  public Result<T> setResult(final T result) {
    this.result = result;
    return this;
  }

  public long getTimestamp() {
    return timestamp;
  }

  public Result<T> setTimestamp(long timestamp) {
    this.timestamp = timestamp;
    return this;
  }
}
