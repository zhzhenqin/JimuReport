package com.jeecg.modules.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * <pre>
 *
 * Created by zhenqin.
 * User: zhenqin
 * Date: 2022/12/6
 * Time: 下午6:16
 * Vendor: yiidata.com
 *
 * </pre>
 *
 * @author zhenqin
 */
@Setter
@Getter
public class ApiSelectVO {

    /**
     * API 选择 ID
     */
    String apiSelectId;
}
