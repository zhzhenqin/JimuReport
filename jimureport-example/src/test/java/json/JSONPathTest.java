package json;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.ReadContext;
import com.jayway.jsonpath.TypeRef;
import com.jayway.jsonpath.spi.json.JacksonJsonProvider;
import com.jayway.jsonpath.spi.json.JsonProvider;
import com.jayway.jsonpath.spi.mapper.JacksonMappingProvider;
import com.jayway.jsonpath.spi.mapper.MappingProvider;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <pre>
 *
 * Created by zhenqin.
 * User: zhenqin
 * Date: 2023/1/5
 * Time: 下午7:50
 * Vendor: yiidata.com
 *
 * </pre>
 *
 * @author zhenqin
 */
public class JSONPathTest {

    @Before
    public void setUp() throws Exception {
        com.jayway.jsonpath.Configuration.setDefaults(new com.jayway.jsonpath.Configuration.Defaults() {

            private final JsonProvider jsonProvider = new JacksonJsonProvider();
            private final MappingProvider mappingProvider = new JacksonMappingProvider();

            @Override
            public JsonProvider jsonProvider() {
                return jsonProvider;
            }

            @Override
            public MappingProvider mappingProvider() {
                return mappingProvider;
            }

            @Override
            public Set<Option> options() {
                return EnumSet.noneOf(Option.class);
            }
        });
        com.jayway.jsonpath.Configuration.defaultConfiguration();
    }

    @Test
    public void testPath() throws Exception {
        String json = "{\n" +
                "    \"data\": [\n" +
                "        {\n" +
                "            \"ctotal\": \"125箱\",\n" +
                "            \"cname\": \"牛奶0\",\n" +
                "            \"cprice\": \"56\",\n" +
                "            \"riqi\": \"2022年10月21日\",\n" +
                "            \"id\": \"1\",\n" +
                "            \"dtotal\": \"1256箱\",\n" +
                "            \"tp\": \"7000\",\n" +
                "            \"ztotal\": \"589箱\",\n" +
                "            \"cnum\": \"每箱12瓶\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"ctotal\": \"126箱\",\n" +
                "            \"cname\": \"牛奶1\",\n" +
                "            \"cprice\": \"56\",\n" +
                "            \"riqi\": \"2022年10月21日\",\n" +
                "            \"id\": \"1\",\n" +
                "            \"dtotal\": \"1256箱\",\n" +
                "            \"tp\": \"7000\",\n" +
                "            \"ztotal\": \"589箱\",\n" +
                "            \"cnum\": \"每箱12瓶\"\n" +
                "        }\n" +
                "    ]\n" +
                "}";
        ReadContext context = JsonPath.parse(json);
        //在这里，“$”中为需要获取到json的路径
        List<Map> result = context.read(getJsonPathString("data"), new TypeRef<List<Map>>() {});
        System.out.println(result);
    }


    private String getJsonPathString(String dataJsonPath) {
        String stdDataJsonPath = dataJsonPath.trim();
        if (StringUtils.isNotBlank(stdDataJsonPath)) {
            // 转换"stores[0].books"、"[1].stores"简化模式为规范的JSONPath
            if (!stdDataJsonPath.startsWith("$")) {
                if (stdDataJsonPath.startsWith("[")) {
                    stdDataJsonPath = "$" + stdDataJsonPath;
                } else {
                    stdDataJsonPath = "$." + stdDataJsonPath;
                }
            }
        }
        return stdDataJsonPath;
    }
}
